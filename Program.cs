﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PojoToPocoConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Converter.BatchConvert(@"c:\model", @"c:\c-model", "Com.Poynt.Api.Model");
        }
    }

    public class Converter
    {
        private static Dictionary<string, string> JavaToCSharpMap = null;
        public static void BatchConvert(string sourcePath, string destinationPath, string csharpNamespace)
        {
            var filesWithPath = Directory.GetFiles(sourcePath);
            List<string> fileNames = new List<string>();
            foreach (var file in filesWithPath)
            {
                fileNames.Add(Path.GetFileName(file));
            }
            var classNames = new Dictionary<string, List<string>>();

            foreach (var fileName in fileNames)
            {
                var className = fileName.Replace(".java", string.Empty);
                var types = Convert(sourcePath, destinationPath, className, csharpNamespace);
                classNames.Add(className, types);
                Console.WriteLine("Added {0} class names", className);
            }

            var ok = true;

            while (ok)
            {
                ok = false;

                foreach (var key in classNames.Keys)
                {
                    string type = null;

                    foreach (var currentType in classNames[key])
                    {
                        if (classNames.ContainsKey(currentType))
                        {
                            type = currentType;
                            break;
                        }
                    }

                    if (type != null)
                    {
                        classNames[key].Remove(type);
                        ok = true;
                    }
                }
            }

            foreach (var key in classNames.Keys)
            {
                if (classNames[key].Count > 0)
                {
                    Convert(sourcePath, destinationPath, key, csharpNamespace, classNames[key]);
                }
            }
        }

        public static List<string> Convert(string sourcePath, string destinationPath, string className, string csharpNamespace, List<string> excludedTypes = null)
        {
            Console.WriteLine("Staring conversion for {0}", className);
            string tabs = string.Empty;
            var types = new List<string>();

            InitializeJavaToCSharpTypes();

            using (var reader = new StreamReader(string.Format(@"{0}\{1}{2}", sourcePath, className, ".java")))
            {
                using (var writer = new StreamWriter(string.Format(@"{0}\{1}{2}", destinationPath, className, ".cs")))
                {
                    writer.WriteLine("using System;");
                    writer.WriteLine("using System.Collections;");
                    writer.WriteLine("using System.Collections.Generic;");
                    writer.WriteLine("using System.Globalization;");
                    writer.WriteLine("using System.Runtime.Serialization;");
                    writer.WriteLine();
                    writer.WriteLine("namespace {0}", csharpNamespace);
                    writer.WriteLine("{");

                    string line;
                    bool enumProcessing = false;
                    bool stop = false;

                    while ((line = reader.ReadLine()) != null && !stop)
                    {
                        if (line.Trim().StartsWith("@")) continue;

                        //Remove inline comments
                        if(line.TrimEnd().Contains("//"))
                        {
                            line = line.Substring(0, line.Length - (line.Length - line.IndexOf("//")));
                        }

                        if (line.Contains("public class"))
                        {
                            writer.WriteLine("\t[DataContract]");
                            writer.WriteLine(string.Concat("\tpublic class ", className));
                            writer.WriteLine("\t{");
                            continue;
                        }

                        //Process enums
                        if (line.Contains("public enum") || enumProcessing)
                        {
                            if(line.Contains("}"))
                            {
                                stop = true;
                                continue;
                            }

                            if (string.IsNullOrEmpty(line.Trim()) || line.Trim().StartsWith("/")) continue;

                            var processedLine = line;

                            if (line.Contains("public enum"))
                            {
                                enumProcessing = true;

                                processedLine = processedLine.TrimEnd();
                                processedLine = processedLine.TrimEnd('{');

                                //to simplify serialiaztion, enums will be converted to classes with static strings
                                //processedLine = processedLine.Replace("enum", "class");
                            }
                            else
                            {
                                //This is enum value -- let's transform
                                //We can't assume, that there is just one value per line

                                //Remove anything between (...)
                                //Regex myRegex = new Regex(@"\(([^\}]+)\)");
                                //processedLine = myRegex.Replace(processedLine, string.Empty);

                                if(processedLine.Contains('('))
                                {
                                    int mode = 0; //-1 == delete
                                    StringBuilder sb = new StringBuilder();
                                    for(int position = 0; position<processedLine.Length; position++)
                                    {
                                        string currentChar = processedLine.Substring(position, 1);
                                        if (currentChar == "(") mode = -1;

                                        if(mode == 0) //we include the character
                                        {
                                            sb.Append(currentChar);
                                        }

                                        if (currentChar == ")") mode = 0;
                                    }

                                    processedLine = sb.ToString();
                                }

                                processedLine = processedLine.Trim();

                                var enumValues = processedLine.Split(',');

                                //Now process each value
                                foreach (var ev in enumValues)
                                {
                                    if (string.IsNullOrEmpty(ev)) continue;

                                    string enumValue = ev.Trim();
                                    enumValue = enumValue.Replace(";", string.Empty);
                                    enumValue = enumValue.Trim();

                                    if (enumValue.Contains("("))
                                    {
                                        //This is an enum with additional decorations. For example, DISCOVER("D")
                                        //We are going to remove annotation and make the enum into a static string where enum value 
                                        //becomes string name and value
                                        enumValue = enumValue.Substring(0, enumValue.Length - (enumValue.Length - enumValue.IndexOf("(")));
                                    }

                                    //Some of the enums had ; on the new line -- this messes up the logic in the script
                                    if (!string.IsNullOrEmpty(enumValue))
                                    {
                                        //writer.WriteLine(string.Format("\t\tpublic static string {0} = \"{0}\";", enumValue));
                                        writer.WriteLine(string.Format("\t\t{0},", enumValue));
                                    }                                  
                                }

                                if(processedLine.TrimEnd().EndsWith(";"))
                                {
                                    stop = true;
                                }

                                continue;
                            }

                            foreach (var mapItem in JavaToCSharpMap)
                            {
                                processedLine = processedLine.Replace(mapItem.Key, mapItem.Value);
                            }

                            writer.WriteLine("\t" + processedLine);
                            if (line.TrimEnd().EndsWith("{"))
                            {
                                writer.WriteLine("\t{");
                            }

                        }
                        else
                        {
                            //Process properties
                            //We are only interested in getting the properties out of the Java class
                            //Let's look for a getter and use it as an ancor to create a property
                            //in the c# class
                            if (line.Contains("public "))
                            {
                                if ((line.Contains(" get") || line.Contains(" is") && line.Contains("()")))
                                {
                                    //this is a getter
                                    //Let's get the type

                                    string propertyPrefix;

                                    if (line.Contains("get"))
                                    {
                                        propertyPrefix = " get";
                                    }
                                    else
                                    {
                                        propertyPrefix = " is";
                                    }

                                    string getterType = line.Substring(line.IndexOf("public") + "public".Length + 1, line.IndexOf(propertyPrefix) - "public ".Length).TrimEnd();

                                    foreach (var mapItem in JavaToCSharpMap)
                                    {
                                        getterType = getterType.Replace(mapItem.Key, mapItem.Value);
                                    }

                                    //We should now have our getter type converted to c# equivalent
                                    //Now we need to extract property name
                                    string propertyName = line.Substring(line.IndexOf(propertyPrefix) + propertyPrefix.TrimEnd().Length, line.IndexOf("()") - (line.IndexOf(propertyPrefix) + propertyPrefix.TrimEnd().Length));

                                    //Now we can add the property to the .cs file
                                    //First, change the case
                                    propertyName = propertyName.First().ToString().ToLower() + propertyName.Substring(1);


                                    //Very special case --- fixed is a .net type and we need to rename it
                                    if (propertyName.Equals("fixed"))
                                    {
                                        propertyName = "fixedDiscount";
                                    }

                                    //Build the property
                                    string transformedProperty = string.Format("\t\tpublic {0} {1} {{get; set;}}", getterType, propertyName);
                                    writer.WriteLine("\t\t[DataMember(IsRequired = false)]");
                                    writer.WriteLine(transformedProperty);
                                }
                            }
                        }

                    }

                    //Add closing brackets
                    writer.WriteLine("\t}");
                    writer.WriteLine("}");
                }
            }

            return types;
        }

        private static void InitializeJavaToCSharpTypes()
        {
            if (JavaToCSharpMap != null)
            {
                return;
            }

            JavaToCSharpMap = new Dictionary<string, string>();
            JavaToCSharpMap.Add("boolean", "bool");
            JavaToCSharpMap.Add("Boolean", "bool?");
            JavaToCSharpMap.Add("byte", "byte");
            JavaToCSharpMap.Add("Byte", "sbyte?");
            JavaToCSharpMap.Add("short", "short?");
            JavaToCSharpMap.Add("Short", "short?");
            JavaToCSharpMap.Add("int", "int");
            JavaToCSharpMap.Add("Integer", "int?");
            JavaToCSharpMap.Add("long", "long");
            JavaToCSharpMap.Add("Long", "long?");
            JavaToCSharpMap.Add("char", "char");
            JavaToCSharpMap.Add("Character", "char?");
            JavaToCSharpMap.Add("float", "float");
            JavaToCSharpMap.Add("Float", "float?");
            JavaToCSharpMap.Add("double", "double");
            JavaToCSharpMap.Add("Double", "double?");
            JavaToCSharpMap.Add("String", "string");
            JavaToCSharpMap.Add("Date", "DateTime");
            JavaToCSharpMap.Add("HashMap", "Hashtable");
            JavaToCSharpMap.Add("Map", "Dictionary");
            JavaToCSharpMap.Add("UUID", "Guid");
            JavaToCSharpMap.Add("BigDecimal", "double");
            JavaToCSharpMap.Add("ArrayList", "List");
            JavaToCSharpMap.Add("Calendar", "DateTimeOffset");

        }

        public static string CapitalizeFirstLetter(string text)
        {
            return string.Concat(text.Substring(0, 1).ToUpper(), text.Substring(1));
        }
    }
}
